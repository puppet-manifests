# A lot of ideas here are found throughout the web
# especiall DavidS github.com/DavidS
# necessary defaults
Exec { path => "/usr/bin:/bin:/usr/sbin:/sbin" }

# Default backup
filebucket { server: server => "dune.lan0" }

File { backup => server }

# allow munin access from the central munin server
$munin_cidr_allow = [ '127.0.0.1/32' ]

#default multimedia repos to no.
#Activate per node if required.
$multimedia_repos = "no"

# import common module first to get all variables to the other modules
import 'common'

# import all other modules
import 'dbp'
#jmrimport 'apache'
#jmrimport 'munin'
#jmrimport 'nagios'
#jmrimport 'ssh'

#######################################################################
##  Nodes  ############################################################
#######################################################################

node default
{
	# all hosts should have the munin agent installed
	#jmr include munin::client

	# watch all hosts
	#jmr include nagios::target

	# every host should be reachable by ssh
	# this is automatically checked with nagios
	include ssh::server
}
node 'debian-default' inherits default
{
    # all debian hosts should inherit this.
#    import 'dbp'
#    include dbp
}

node 'fw.lan0' inherits debian-default
{
    $lsbdistcodename = "lenny"
    include dbp
}

node 'dune.lan0' inherits debian-default
{
    $lsbdistcodename = "lenny"
    $multimedia_repos = "yes"
    include dbp
}

node 'sylvester.lan0' inherits debian-default
{
    $lsbdistcodename = "sid"
    $multimedia_repos = "yes"
    include dbp
}

node 'tst.lan0' inherits debian-default
{
    $lsbdistcodename = "lenny"
    $multimedia_repos = "yes"
    include dbp
}

node 'tweety.lan0' inherits debian-default
{
    $lsbdistcodename = "sid"
    $multimedia_repos = "yes"
    include dbp
}
